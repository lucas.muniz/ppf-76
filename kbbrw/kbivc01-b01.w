&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          mgesp            PROGRESS
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS B-table-Win 
/*:T *******************************************************************************
** Copyright TOTVS S.A. (2009)
** Todos os Direitos Reservados.
**
** Este fonte e de propriedade exclusiva da TOTVS, sua reproducao
** parcial ou total por qualquer meio, so podera ser feita mediante
** autorizacao expressa.
*******************************************************************************/
{include/i-prgvrs.i B99XX999 9.99.99.999}

/* Chamada a include do gerenciador de licen�as. Necessario alterar os parametros */
/*                                                                                */
/* <programa>:  Informar qual o nome do programa.                                 */
/* <m�dulo>:  Informar qual o m�dulo a qual o programa pertence.                  */
/*                                                                                */
/* OBS: Para os smartobjects o parametro m�dulo dever� ser MUT                    */

&IF "{&EMSFND_VERSION}" >= "1.00" &THEN
    {include/i-license-manager.i <programa> MUT}
&ENDIF

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */
&Scop adm-attribute-dlg support/browserd.w

/* Parameters Definitions ---                                           */
 
/* Local Variable Definitions ---                                       */

DEF TEMP-TABLE tt-kb_email_inad_param_tarefa LIKE kb_email_inad_param_tarefa
    FIELD prazo-texto        AS CHAR FORMAT "x(30)"
    FIELD mensagem-descricao AS CHAR FORMAT "x(30)"
    FIELD r-rowid            AS ROWID
    FIELD des_msg_abrev      AS CHAR FORMAT "x(100)"
    FIELD des_classe         AS CHAR FORMAT "x(100)".

/*:T Variaveis usadas internamente pelo estilo, favor nao elimina-las     */

/*:T v�ri�veis de uso globla */
def  var v-row-parent    as rowid no-undo.

/*:T vari�veis de uso local */
def var v-row-table  as rowid no-undo.

/*:T fim das variaveis utilizadas no estilo */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE BrowserCadastro2
&Scoped-define DB-AWARE no

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME F-Main
&Scoped-define BROWSE-NAME br-table

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES kb_email_inad_param
&Scoped-define FIRST-EXTERNAL-TABLE kb_email_inad_param


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR kb_email_inad_param.
/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES tt-kb_email_inad_param_tarefa

/* Define KEY-PHRASE in case it is used by any query. */
&Scoped-define KEY-PHRASE TRUE

/* Definitions for BROWSE br-table                                      */
&Scoped-define FIELDS-IN-QUERY-br-table tt-kb_email_inad_param_tarefa.ativo tt-kb_email_inad_param_tarefa.descricao tt-kb_email_inad_param_tarefa.qtd_dias tt-kb_email_inad_param_tarefa.prazo-texto tt-kb_email_inad_param_tarefa.des_classe tt-kb_email_inad_param_tarefa.des_msg_abrev tt-kb_email_inad_param_tarefa.mensagem-descricao   
&Scoped-define ENABLED-FIELDS-IN-QUERY-br-table   
&Scoped-define SELF-NAME br-table
&Scoped-define QUERY-STRING-br-table FOR EACH tt-kb_email_inad_param_tarefa OF kb_email_inad_param NO-LOCK     ~{&SORTBY-PHRASE} INDEXED-REPOSITION
&Scoped-define OPEN-QUERY-br-table OPEN QUERY {&SELF-NAME} FOR EACH tt-kb_email_inad_param_tarefa OF kb_email_inad_param NO-LOCK     ~{&SORTBY-PHRASE} INDEXED-REPOSITION.
&Scoped-define TABLES-IN-QUERY-br-table tt-kb_email_inad_param_tarefa
&Scoped-define FIRST-TABLE-IN-QUERY-br-table tt-kb_email_inad_param_tarefa


/* Definitions for FRAME F-Main                                         */

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS br-table bt-incluir bt-modificar bt-eliminar 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Foreign Keys" B-table-Win _INLINE
/* Actions: ? adm/support/keyedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
&BROWSE-NAME
</KEY-OBJECT>
<FOREIGN-KEYS>
cod-emitente||y|mgesp.kb_email_inad_param_tarefa.cod-emitente
</FOREIGN-KEYS> 
<EXECUTING-CODE>
**************************
* Set attributes related to FOREIGN KEYS
*/
RUN set-attribute-list (
    'Keys-Accepted = ,
     Keys-Supplied = "cod-emitente"':U).

/* Tell the ADM to use the OPEN-QUERY-CASES. */
&Scoped-define OPEN-QUERY-CASES RUN dispatch ('open-query-cases':U).
/**************************
</EXECUTING-CODE> */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Advanced Query Options" B-table-Win _INLINE
/* Actions: ? adm/support/advqedit.w ? ? ? */
/* STRUCTURED-DATA
<KEY-OBJECT>
&BROWSE-NAME
</KEY-OBJECT>
<SORTBY-OPTIONS>
</SORTBY-OPTIONS>
<SORTBY-RUN-CODE>
************************
* Set attributes related to SORTBY-OPTIONS */
RUN set-attribute-list (
    'SortBy-Options = ""':U).
/************************
</SORTBY-RUN-CODE>
<FILTER-ATTRIBUTES>
************************
* Initialize Filter Attributes */
RUN set-attribute-list IN THIS-PROCEDURE ('
  Filter-Value=':U).
/************************
</FILTER-ATTRIBUTES> */   

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD retorna_desc B-table-Win 
FUNCTION retorna_desc RETURNS CHARACTER (INPUT cod-msg AS CHAR)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD retorna_desc_classe B-table-Win 
FUNCTION retorna_desc_classe RETURNS CHARACTER (INPUT cod_classif_msg_cobr AS CHAR)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD retorna_desc_msg B-table-Win 
FUNCTION retorna_desc_msg RETURNS CHARACTER
  (INPUT cod_empresa_msg AS CHAR,
   INPUT cod_estab_msg   AS CHAR,
   INPUT cod_mensagem    AS CHAR)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD retorna_nome B-table-Win 
FUNCTION retorna_nome RETURNS CHARACTER (INPUT prazo AS INTEGER) FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD teste B-table-Win 
FUNCTION teste RETURNS CHARACTER
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE BUTTON bt-eliminar 
     LABEL "&Excluir" 
     SIZE 10 BY 1.

DEFINE BUTTON bt-incluir 
     LABEL "&Incluir" 
     SIZE 10 BY 1.

DEFINE BUTTON bt-modificar 
     LABEL "&Alterar" 
     SIZE 10 BY 1.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY br-table FOR 
      tt-kb_email_inad_param_tarefa SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE br-table
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS br-table B-table-Win _FREEFORM
  QUERY br-table NO-LOCK DISPLAY
      tt-kb_email_inad_param_tarefa.ativo COLUMN-LABEL "" FORMAT "yes/no":U
            VIEW-AS TOGGLE-BOX
      tt-kb_email_inad_param_tarefa.descricao COLUMN-LABEL "Tarefas R�gua de Cobran�a" FORMAT "x(50)":U
            WIDTH 34.29
      tt-kb_email_inad_param_tarefa.qtd_dias COLUMN-LABEL "Dias" FORMAT "->,>>>,>>9":U
            WIDTH 5.43
      tt-kb_email_inad_param_tarefa.prazo-texto COLUMN-LABEL "Prazo"
            WIDTH 17
      tt-kb_email_inad_param_tarefa.des_classe COLUMN-LABEL "Classe Msg" FORMAT "x(100)":U
            WIDTH 12.43
      tt-kb_email_inad_param_tarefa.des_msg_abrev COLUMN-LABEL "Mensagem" FORMAT "x(100)":U
            WIDTH 11.43
      tt-kb_email_inad_param_tarefa.mensagem-descricao COLUMN-LABEL "-"
            WIDTH 19.43
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ASSIGN SEPARATORS SIZE 110 BY 7.75 ROW-HEIGHT-CHARS .67.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     br-table AT ROW 1 COL 1
     bt-incluir AT ROW 9 COL 79
     bt-modificar AT ROW 9 COL 90
     bt-eliminar AT ROW 9 COL 101
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         BGCOLOR 8 FGCOLOR 0  WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: BrowserCadastro2
   External Tables: mgesp.kb_email_inad_param
   Allow: Basic,Browse
   Frames: 1
   Add Fields to: External-Tables
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW B-table-Win ASSIGN
         HEIGHT             = 9.25
         WIDTH              = 110.14.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB B-table-Win 
/* ************************* Included-Libraries *********************** */

{utp/ut-glob.i}
{src/adm/method/browser.i}
{include/c-brows3.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW B-table-Win
  NOT-VISIBLE,,RUN-PERSISTENT                                           */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE FRAME-NAME Size-to-Fit L-To-R                            */
/* BROWSE-TAB br-table 1 F-Main */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE br-table
/* Query rebuild information for BROWSE br-table
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH tt-kb_email_inad_param_tarefa OF kb_email_inad_param NO-LOCK
    ~{&SORTBY-PHRASE} INDEXED-REPOSITION.
     _END_FREEFORM
     _Options          = "NO-LOCK INDEXED-REPOSITION KEY-PHRASE SORTBY-PHRASE"
     _Query            is NOT OPENED
*/  /* BROWSE br-table */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define BROWSE-NAME br-table
&Scoped-define SELF-NAME br-table
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br-table B-table-Win
ON MOUSE-SELECT-DBLCLICK OF br-table IN FRAME F-Main
DO:
    RUN New-State("DblClick, SELF":U).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br-table B-table-Win
ON ROW-ENTRY OF br-table IN FRAME F-Main
DO:
  /* This code displays initial values for newly added or copied rows. */
  {src/adm/template/brsentry.i}  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br-table B-table-Win
ON ROW-LEAVE OF br-table IN FRAME F-Main
DO:
    /* Do not disable this code or no updates will take place except
     by pressing the Save button on an Update SmartPanel. */
   {src/adm/template/brsleave.i}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br-table B-table-Win
ON VALUE-CHANGED OF br-table IN FRAME F-Main
DO:
  /* This ADM trigger code must be preserved in order to notify other
     objects when the browser's current row changes. */
  {src/adm/template/brschnge.i}
  /* run new-state('New-Line|':U + string(rowid({&FIRST-TABLE-IN-QUERY-{&BROWSE-NAME}}))). */
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-eliminar
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-eliminar B-table-Win
ON CHOOSE OF bt-eliminar IN FRAME F-Main /* Excluir */
DO:
/*     FIND CURRENT kb_email_inad_param NO-LOCK NO-ERROR.  */

    IF AVAIL kb_email_inad_param THEN DO:
        FIND CURRENT tt-kb_email_inad_param_tarefa EXCLUSIVE-LOCK NO-ERROR.
        IF AVAIL tt-kb_email_inad_param_tarefa THEN DO:
            MESSAGE "Deseja excluir a tarefa selecionada?"
                VIEW-AS ALERT-BOX QUESTION BUTTONS YES-NO
                TITLE "" UPDATE lChoice AS LOGICAL.
            IF lChoice THEN DO:
                FIND FIRST kb_email_inad_param_tarefa WHERE ROWID(kb_email_inad_param_tarefa) = tt-kb_email_inad_param_tarefa.r-rowid.
                IF AVAIL kb_email_inad_param_tarefa THEN DO:
                    DELETE kb_email_inad_param_tarefa.
                    MESSAGE "Tarefa deletada com sucesso!"
                        VIEW-AS ALERT-BOX INFO BUTTONS OK.
                END.
                ELSE DO:
                    MESSAGE "ROWID do registro � inv�lido."
                        VIEW-AS ALERT-BOX INFO BUTTONS OK.
                END.
            END.
            ELSE DO:
                MESSAGE "Opera��o cancelada."
                    VIEW-AS ALERT-BOX INFO BUTTONS OK.
            END.
        END.
        ELSE DO:
            MESSAGE "Tarefa n�o dispon�vel para dele��o."
                VIEW-AS ALERT-BOX INFO BUTTONS OK.
        END.
    END.

    ELSE DO:
        MESSAGE "Registro de par�metros de e-mail n�o dispon�vel."
            VIEW-AS ALERT-BOX WARNING BUTTONS OK.
    END.

   RUN pi-eliminar.

   RUN pi-manipula-tarefa.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-incluir
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-incluir B-table-Win
ON CHOOSE OF bt-incluir IN FRAME F-Main /* Incluir */
DO:
    FIND CURRENT kb_email_inad_param NO-LOCK NO-ERROR.

    IF AVAIL kb_email_inad_param THEN DO:
        RUN kbvwr\kbivc01-v02.w(INPUT ?, INPUT ROWID(kb_email_inad_param)).
    END.
    


    ELSE DO:

        MESSAGE "Registro de par�metros de e-mail n�o dispon�vel."
            VIEW-AS ALERT-BOX WARNING BUTTONS OK.
    END.

  RUN pi-Incmod ('incluir':U).

  RUN pi-manipula-tarefa.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME bt-modificar
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL bt-modificar B-table-Win
ON CHOOSE OF bt-modificar IN FRAME F-Main /* Alterar */
DO:
/*     FIND CURRENT kb_email_inad_param NO-LOCK NO-ERROR.  */

    IF AVAIL kb_email_inad_param THEN DO:

        FIND CURRENT tt-kb_email_inad_param_tarefa EXCLUSIVE-LOCK NO-ERROR.

        IF AVAIL tt-kb_email_inad_param_tarefa THEN DO:
            RUN kbvwr\kbivc01-v02.w(INPUT tt-kb_email_inad_param_tarefa.r-rowid, INPUT ROWID(kb_email_inad_param)).
        END.

        ELSE DO:
            MESSAGE "Tarefa n�o dispon�vel para altera��o."
                VIEW-AS ALERT-BOX INFO BUTTONS OK.
        END.
    END.

    ELSE DO:
        MESSAGE "Registro de par�metros de e-mail n�o dispon�vel."
            VIEW-AS ALERT-BOX WARNING BUTTONS OK.
    END.

  RUN pi-Incmod ('modificar':U).

  RUN pi-manipula-tarefa.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK B-table-Win 


/* ***************************  Main Block  *************************** */

&IF DEFINED(UIB_IS_RUNNING) <> 0 &THEN          
RUN dispatch IN THIS-PROCEDURE ('initialize':U).        
&ENDIF

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-open-query-cases B-table-Win  adm/support/_adm-opn.p
PROCEDURE adm-open-query-cases :
/*------------------------------------------------------------------------------
  Purpose:     Opens different cases of the query based on attributes
               such as the 'Key-Name', or 'SortBy-Case'
  Parameters:  <none>
------------------------------------------------------------------------------*/
  DEF VAR Filter-Value AS CHAR NO-UNDO.

  /* Copy 'Filter-Attributes' into local variables. */
  RUN get-attribute ('Filter-Value':U).
  Filter-Value = RETURN-VALUE.

  /* No Foreign keys are accepted by this SmartObject. */

  {&OPEN-QUERY-{&BROWSE-NAME}}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available B-table-Win  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "kb_email_inad_param"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "kb_email_inad_param"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI B-table-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-display-fields B-table-Win 
PROCEDURE local-display-fields :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

RUN pi-manipula-tarefa.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-view B-table-Win 
PROCEDURE local-view :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'view':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
  apply 'value-changed':U to {&browse-name} in frame {&frame-name}.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pi-manipula-tarefa B-table-Win 
PROCEDURE pi-manipula-tarefa :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
EMPTY TEMP-TABLE tt-kb_email_inad_param_tarefa.

FOR EACH kb_email_inad_param_tarefa OF kb_email_inad_param:
    CREATE tt-kb_email_inad_param_tarefa.
    BUFFER-COPY kb_email_inad_param_tarefa TO tt-kb_email_inad_param_tarefa.
    ASSIGN tt-kb_email_inad_param_tarefa.prazo-texto        = retorna_nome(INPUT tt-kb_email_inad_param_tarefa.prazo)
           tt-kb_email_inad_param_tarefa.mensagem-descricao = retorna_desc(INPUT tt-kb_email_inad_param_tarefa.cod_classif_msg_cobr)
           tt-kb_email_inad_param_tarefa.r-rowid            = ROWID(kb_email_inad_param_tarefa)
           tt-kb_email_inad_param_tarefa.des_msg_abrev      = retorna_desc_msg(INPUT tt-kb_email_inad_param_tarefa.cod_empresa_msg, INPUT tt-kb_email_inad_param_tarefa.cod_estab_msg, INPUT tt-kb_email_inad_param_tarefa.cod_mensagem)
           tt-kb_email_inad_param_tarefa.des_classe         = retorna_desc_classe(INPUT tt-kb_email_inad_param_tarefa.cod_classif_msg_cobr).
END.                                                                           

{&open-query-br-table}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-key B-table-Win  adm/support/_key-snd.p
PROCEDURE send-key :
/*------------------------------------------------------------------------------
  Purpose:     Sends a requested KEY value back to the calling
               SmartObject.
  Parameters:  <see adm/template/sndkytop.i>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/sndkytop.i}

  /* Return the key value associated with each key case.             */
  {src/adm/template/sndkycas.i "cod-emitente" "kb_email_inad_param_tarefa" "cod-emitente"}

  /* Close the CASE statement and end the procedure.                 */
  {src/adm/template/sndkyend.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records B-table-Win  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "kb_email_inad_param"}
  {src/adm/template/snd-list.i "tt-kb_email_inad_param_tarefa"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed B-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {src/adm/template/bstates.i}
  END CASE.
  run pi-trata-state (p-issuer-hdl, p-state).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION retorna_desc B-table-Win 
FUNCTION retorna_desc RETURNS CHARACTER (INPUT cod-msg AS CHAR) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

    FIND FIRST classif_msg_cobr WHERE 
        classif_msg_cobr.cod_classif_msg_cobr = cod-msg NO-LOCK NO-ERROR.

    IF AVAIL classif_msg_cobr THEN
        RETURN classif_msg_cobr.des_classif_msg_cobr.   /* Function return value. */

    ELSE RETURN "".
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION retorna_desc_classe B-table-Win 
FUNCTION retorna_desc_classe RETURNS CHARACTER (INPUT cod_classif_msg_cobr AS CHAR) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  FIND FIRST classif_msg_cobr 
      WHERE classif_msg_cobr.cod_classif_msg_cobr = cod_classif_msg_cobr NO-LOCK NO-ERROR.

  IF AVAIL classif_msg_cobr THEN DO:
      RETURN classif_msg_cobr.des_classif_msg_cobr.
  END.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION retorna_desc_msg B-table-Win 
FUNCTION retorna_desc_msg RETURNS CHARACTER
  (INPUT cod_empresa_msg AS CHAR,
   INPUT cod_estab_msg   AS CHAR,
   INPUT cod_mensagem    AS CHAR) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

    FIND FIRST msg_financ WHERE
        msg_financ.cod_empresa  = cod_empresa_msg AND
        msg_financ.cod_estab    = cod_estab_msg AND
        msg_financ.cod_mensagem = cod_mensagem NO-LOCK NO-ERROR.

    IF AVAIL msg_financ THEN DO:
        RETURN msg_financ.des_msg_abrev.
    END.  

    ELSE RETURN "".
END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION retorna_nome B-table-Win 
FUNCTION retorna_nome RETURNS CHARACTER (INPUT prazo AS INTEGER):
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
    IF prazo = 0 THEN
        RETURN "N�o se aplica".

    ELSE IF prazo = 1 THEN
        RETURN "Antes do vencimento".
    
    ELSE IF prazo = 2 THEN
        RETURN "Depois do vencimento".

 

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION teste B-table-Win 
FUNCTION teste RETURNS CHARACTER
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  RETURN "".   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

