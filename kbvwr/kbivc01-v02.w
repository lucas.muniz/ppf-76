&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI ADM2
&ANALYZE-RESUME
/* Connected Databases 
          mgesp            PROGRESS
*/
&Scoped-define WINDOW-NAME wWin
{adecomm/appserv.i}
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS wWin 
/*------------------------------------------------------------------------

  File: 

  Description: from cntnrwin.w - ADM SmartWindow Template

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  History: New V9 Version - January 15, 1998
          
------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AB.              */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */

/* Parameters Definitions ---                                           */
DEF INPUT PARAM r-rowid       AS ROWID NO-UNDO.
DEF INPUT PARAM r-rowid-param AS ROWID NO-UNDO.
/* Local Variable Definitions ---                                       */
DEF BUFFER b_kb_email_inad_param_tarefa FOR kb_email_inad_param_tarefa.
DEF VAR p-valida-retorno         AS LOGICAL NO-UNDO.
DEF VAR c-selec-msg-retorno      AS CHAR NO-UNDO.
DEF VAR c-selec-msg-desc-retorno AS CHAR NO-UNDO. 
DEF VAR cod-empresa-retorno      AS CHAR NO-UNDO. 
DEF VAR cod-estab-retorno        AS CHAR NO-UNDO.
DEF VAR cod-classe-msg           AS CHAR NO-UNDO.
DEF VAR des-classe-msg           AS CHAR NO-UNDO.
{src/adm2/widgetprto.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartWindow
&Scoped-define DB-AWARE no

&Scoped-define ADM-CONTAINER WINDOW

&Scoped-define ADM-SUPPORTED-LINKS Data-Target,Data-Source,Page-Target,Update-Source,Update-Target,Filter-target,Filter-Source

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME fMain

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES kb_email_inad_param_tarefa

/* Definitions for FRAME fMain                                          */
&Scoped-define QUERY-STRING-fMain FOR EACH kb_email_inad_param_tarefa SHARE-LOCK
&Scoped-define OPEN-QUERY-fMain OPEN QUERY fMain FOR EACH kb_email_inad_param_tarefa SHARE-LOCK.
&Scoped-define TABLES-IN-QUERY-fMain kb_email_inad_param_tarefa
&Scoped-define FIRST-TABLE-IN-QUERY-fMain kb_email_inad_param_tarefa


/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-2 TG-ATIVO descricao CB-Prazo BTN-OK ~
BTN-Cancelar 
&Scoped-Define DISPLAYED-OBJECTS TG-ATIVO descricao CB-Prazo qtd_dias ~
cod_classif_msg_cobr cod_mensagem 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD f-valida wWin 
FUNCTION f-valida RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR wWin AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON BTN-Cancelar AUTO-END-KEY 
     LABEL "Cancelar" 
     SIZE 15 BY 1.13.

DEFINE BUTTON BTN-CLS 
     LABEL "Classes" 
     SIZE 15 BY 1.13.

DEFINE BUTTON BTN-MSG 
     LABEL "Mensagens" 
     SIZE 15 BY 1.13.

DEFINE BUTTON BTN-OK AUTO-GO 
     LABEL "OK" 
     SIZE 15 BY 1.13.

DEFINE VARIABLE CB-Prazo AS INTEGER FORMAT "->,>>>,>>9":U INITIAL 0 
     LABEL "Prazo" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     LIST-ITEM-PAIRS "N�o se aplica",0,
                     "Antes do vencimento",1,
                     "Depois do vencimento",2
     DROP-DOWN-LIST
     SIZE 16 BY 1 NO-UNDO.

DEFINE VARIABLE cod_classif_msg_cobr AS CHARACTER FORMAT "x(100)" INITIAL "0" 
     LABEL "Classe mensagem" 
     VIEW-AS FILL-IN 
     SIZE 35 BY .88 NO-UNDO.

DEFINE VARIABLE cod_mensagem AS CHARACTER FORMAT "x(100)" INITIAL "0" 
     LABEL "Mensagem" 
     VIEW-AS FILL-IN 
     SIZE 35 BY .88 NO-UNDO.

DEFINE VARIABLE descricao LIKE kb_email_inad_param_tarefa.descricao
     LABEL "Descri��o tarefa" 
     VIEW-AS FILL-IN 
     SIZE 51.14 BY .88 NO-UNDO.

DEFINE VARIABLE qtd_dias LIKE kb_email_inad_param_tarefa.qtd_dias
     LABEL "Quantidade dias" 
     VIEW-AS FILL-IN 
     SIZE 12.57 BY .88 NO-UNDO.

DEFINE RECTANGLE RECT-2
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 78 BY 8.75.

DEFINE VARIABLE TG-ATIVO AS LOGICAL INITIAL yes 
     LABEL "Ativo" 
     VIEW-AS TOGGLE-BOX
     SIZE 11.57 BY .83 NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY fMain FOR 
      kb_email_inad_param_tarefa SCROLLING.
&ANALYZE-RESUME

/* ************************  Frame Definitions  *********************** */

DEFINE FRAME fMain
     TG-ATIVO AT ROW 2 COL 23 WIDGET-ID 30
     descricao AT ROW 3.25 COL 21 COLON-ALIGNED HELP
          "" WIDGET-ID 24
          LABEL "Descri��o tarefa"
     CB-Prazo AT ROW 4.5 COL 21 COLON-ALIGNED WIDGET-ID 32
     qtd_dias AT ROW 5.92 COL 21 COLON-ALIGNED HELP
          "" WIDGET-ID 28
          LABEL "Quantidade dias"
     BTN-CLS AT ROW 7 COL 59 WIDGET-ID 38
     cod_classif_msg_cobr AT ROW 7.17 COL 21 COLON-ALIGNED WIDGET-ID 20
     BTN-MSG AT ROW 8.25 COL 59 WIDGET-ID 36
     cod_mensagem AT ROW 8.42 COL 21 COLON-ALIGNED WIDGET-ID 22
     BTN-OK AT ROW 10.25 COL 48 WIDGET-ID 2
     BTN-Cancelar AT ROW 10.25 COL 65 WIDGET-ID 4
     RECT-2 AT ROW 1.25 COL 2 WIDGET-ID 34
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 80 BY 10.58
         BGCOLOR 8 FGCOLOR 0  WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartWindow
   Allow: Basic,Browse,DB-Fields,Query,Smart,Window
   Container Links: Data-Target,Data-Source,Page-Target,Update-Source,Update-Target,Filter-target,Filter-Source
   Other Settings: APPSERVER
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW wWin ASSIGN
         HIDDEN             = YES
         TITLE              = "<insert SmartWindow title>"
         HEIGHT             = 10.58
         WIDTH              = 80
         MAX-HEIGHT         = 42.38
         MAX-WIDTH          = 274.29
         VIRTUAL-HEIGHT     = 42.38
         VIRTUAL-WIDTH      = 274.29
         RESIZE             = no
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB wWin 
/* ************************* Included-Libraries *********************** */

{src/adm2/containr.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW wWin
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME fMain
   FRAME-NAME                                                           */
/* SETTINGS FOR BUTTON BTN-CLS IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON BTN-MSG IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN cod_classif_msg_cobr IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN cod_mensagem IN FRAME fMain
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN descricao IN FRAME fMain
   LIKE = mgesp.kb_email_inad_param_tarefa. EXP-LABEL EXP-SIZE          */
/* SETTINGS FOR FILL-IN qtd_dias IN FRAME fMain
   NO-ENABLE LIKE = mgesp.kb_email_inad_param_tarefa. EXP-LABEL EXP-SIZE */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(wWin)
THEN wWin:HIDDEN = yes.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME fMain
/* Query rebuild information for FRAME fMain
     _TblList          = "mgesp.kb_email_inad_param_tarefa"
     _Query            is OPENED
*/  /* FRAME fMain */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME wWin
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL wWin wWin
ON END-ERROR OF wWin /* <insert SmartWindow title> */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL wWin wWin
ON WINDOW-CLOSE OF wWin /* <insert SmartWindow title> */
DO:
  /* This ADM code must be left here in order for the SmartWindow
     and its descendents to terminate properly on exit. */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BTN-Cancelar
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BTN-Cancelar wWin
ON CHOOSE OF BTN-Cancelar IN FRAME fMain /* Cancelar */
DO:
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BTN-CLS
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BTN-CLS wWin
ON CHOOSE OF BTN-CLS IN FRAME fMain /* Classes */
DO:
    RUN w-pescls.w  (OUTPUT cod-classe-msg,
                   OUTPUT des-classe-msg).
    IF cod-classe-msg <> "" THEN DO:
        ASSIGN INPUT FRAME fMain cod_classif_msg_cobr:SCREEN-VALUE = des-classe-msg.
    END.
    ELSE DO:
        ASSIGN INPUT FRAME fMain cod_classif_msg_cobr:SCREEN-VALUE = "".
    END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BTN-MSG
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BTN-MSG wWin
ON CHOOSE OF BTN-MSG IN FRAME fMain /* Mensagens */
DO:
    RUN w-pesmsg.w  (OUTPUT c-selec-msg-retorno,
                     OUTPUT c-selec-msg-desc-retorno, 
                     OUTPUT cod-empresa-retorno, 
                     OUTPUT cod-estab-retorno).
    IF c-selec-msg-desc-retorno <> "" THEN DO:
        ASSIGN INPUT FRAME fMain cod_mensagem:SCREEN-VALUE = c-selec-msg-desc-retorno.
    END.
    ELSE DO:
        ASSIGN INPUT FRAME fMain cod_mensagem:SCREEN-VALUE = "".
    END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME BTN-OK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL BTN-OK wWin
ON CHOOSE OF BTN-OK IN FRAME fMain /* OK */
DO: 

    IF f-valida() THEN DO:
        IF r-rowid = ? THEN DO:
       
            CREATE kb_email_inad_param_tarefa.
                    
            FIND FIRST kb_email_inad_param 
                WHERE ROWID(kb_email_inad_param) = r-rowid-param
                NO-LOCK NO-ERROR.
       
            ASSIGN kb_email_inad_param_tarefa.cod-emitente    =  kb_email_inad_param.cod-emitente 
                   kb_email_inad_param_tarefa.cod-matriz      =  kb_email_inad_param.cod-matriz   
                   kb_email_inad_param_tarefa.cod-gr-cli      =  kb_email_inad_param.cod-gr-cli   
                   kb_email_inad_param_tarefa.cod_empresa_msg = "0"
                   kb_email_inad_param_tarefa.cod_estab_msg   = "0".
                
            FIND LAST b_kb_email_inad_param_tarefa 
                WHERE b_kb_email_inad_param_tarefa.cod-emitente = kb_email_inad_param.cod-emitente
                AND   b_kb_email_inad_param_tarefa.cod-matriz   = kb_email_inad_param.cod-matriz
                AND   b_kb_email_inad_param_tarefa.cod-gr-cli   = kb_email_inad_param.cod-gr-cli
                NO-LOCK NO-ERROR.
            IF AVAIL b_kb_email_inad_param_tarefa 
                THEN kb_email_inad_param_tarefa.seq = b_kb_email_inad_param_tarefa.seq  + 1.
                ELSE kb_email_inad_param_tarefa.seq = 1.
        
        END.
       
        ASSIGN kb_email_inad_param_tarefa.ativo                = INPUT FRAME fMain TG-ATIVO
               kb_email_inad_param_tarefa.descricao            = descricao:SCREEN-VALUE
               kb_email_inad_param_tarefa.prazo                = INPUT FRAME fMain CB-Prazo
               kb_email_inad_param_tarefa.qtd_dias             = INPUT FRAME fMain qtd_dias
               kb_email_inad_param_tarefa.cod_classif_msg_cobr = cod-classe-msg
               kb_email_inad_param_tarefa.cod_mensagem         = c-selec-msg-retorno
               kb_email_inad_param_tarefa.cod_estab_msg        = cod-estab-retorno
               kb_email_inad_param_tarefa.cod_empresa_msg      = cod-empresa-retorno.

        apply "close":U to this-procedure.
    END.

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME CB-Prazo
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL CB-Prazo wWin
ON VALUE-CHANGED OF CB-Prazo IN FRAME fMain /* Prazo */
DO:
  DO WITH FRAME fMain:
    IF INPUT CB-Prazo = 0 THEN DO:
      qtd_dias:SCREEN-VALUE = "0".
      cod_classif_msg_cobr:SCREEN-VALUE = "0".
      cod_mensagem:SCREEN-VALUE = "0".      
      DISABLE qtd_dias
              BTN-MSG
              BTN-CLS.
  END.
  ELSE DO:
      ENABLE qtd_dias
             BTN-MSG
             BTN-CLS.
  END.
END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK wWin 


/* ***************************  Main Block  *************************** */

/* Include custom  Main Block code for SmartWindows. */

RUN p-inicializa.
{src/adm2/windowmn.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-create-objects wWin  _ADM-CREATE-OBJECTS
PROCEDURE adm-create-objects :
/*------------------------------------------------------------------------------
  Purpose:     Create handles for all SmartObjects used in this procedure.
               After SmartObjects are initialized, then SmartLinks are added.
  Parameters:  <none>
------------------------------------------------------------------------------*/

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI wWin  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(wWin)
  THEN DELETE WIDGET wWin.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI wWin  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/

  {&OPEN-QUERY-fMain}
  GET FIRST fMain.
  DISPLAY TG-ATIVO descricao CB-Prazo qtd_dias cod_classif_msg_cobr cod_mensagem 
      WITH FRAME fMain IN WINDOW wWin.
  ENABLE RECT-2 TG-ATIVO descricao CB-Prazo BTN-OK BTN-Cancelar 
      WITH FRAME fMain IN WINDOW wWin.
  {&OPEN-BROWSERS-IN-QUERY-fMain}
  VIEW wWin.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE exitObject wWin 
PROCEDURE exitObject :
/*------------------------------------------------------------------------------
  Purpose:  Window-specific override of this procedure which destroys 
            its contents and itself.
    Notes:  
------------------------------------------------------------------------------*/

  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE p-inicializa wWin 
PROCEDURE p-inicializa :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DO WITH FRAME fMain:

    ASSIGN qtd_dias:SCREEN-VALUE = "0"
           cod_classif_msg_cobr:SCREEN-VALUE = "0"
           cod_mensagem:SCREEN-VALUE = "0".

    FIND FIRST kb_email_inad_param_tarefa WHERE ROWID(kb_email_inad_param_tarefa) = r-rowid NO-LOCK NO-ERROR.

    IF AVAIL kb_email_inad_param_tarefa THEN DO:

        ASSIGN TG-ATIVO             = kb_email_inad_param_tarefa.ativo
               descricao            = STRING(kb_email_inad_param_tarefa.descricao)
               CB-Prazo             = kb_email_inad_param_tarefa.prazo
               qtd_dias             = kb_email_inad_param_tarefa.qtd_dias
               cod_classif_msg_cobr = STRING(kb_email_inad_param_tarefa.cod_classif_msg_cobr)
               cod_mensagem         = STRING(kb_email_inad_param_tarefa.cod_mensagem).
    END.
       
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION f-valida wWin 
FUNCTION f-valida RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  FIND FIRST classif_msg_cobr WHERE classif_msg_cobr.cod_classif_msg_cobr = cod-classe-msg NO-LOCK NO-ERROR.
  FIND FIRST msg_financ WHERE msg_financ.cod_mensagem = c-selec-msg-retorno NO-LOCK NO-ERROR.

  IF r-rowid <>  ? THEN DO:

      FIND FIRST kb_email_inad_param_tarefa WHERE ROWID(kb_email_inad_param_tarefa) = r-rowid EXCLUSIVE-LOCK NO-ERROR.
      IF NOT AVAIL kb_email_inad_param_tarefa THEN DO:

          MESSAGE "Erro ao encontrar o registro no banco."
              VIEW-AS ALERT-BOX ERROR BUTTONS OK.
          RETURN FALSE.

      END.
  END.
  
  IF INPUT FRAME fMain descricao = ? 
  OR TRIM(INPUT FRAME fMain descricao) = ""
  THEN DO:
     MESSAGE "A descri��o da tarefa � obrigat�ria."
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.

      RETURN FALSE.
  END.
  ELSE DO:

        IF INPUT FRAME fMain CB-Prazo:SCREEN-VALUE <> "0" THEN DO:

            IF AVAIL classif_msg_cobr AND AVAIL msg_financ THEN DO:

                IF INPUT FRAME fMain CB-Prazo:SCREEN-VALUE = "1" AND INPUT FRAME fMain qtd_dias < "0" THEN DO:

                    MESSAGE "Quantidade de dias deve ser maior ou igual a zero caso o prazo seja antes do vencimento."
                        VIEW-AS ALERT-BOX ERROR BUTTONS OK.

                    RETURN FALSE.
                END.   
                ELSE IF INPUT FRAME fMain CB-Prazo = "2" AND INPUT FRAME fMain qtd_dias < "1" THEN DO:
                    
                    MESSAGE "Quantidade de dias deve ser maior ou igual a 1 caso o prazo seja depois do vencimento."
                        VIEW-AS ALERT-BOX ERROR BUTTONS OK.

                    RETURN FALSE.
                END.
            END.

            ELSE DO:

                MESSAGE "Classe mensagem e c�digo mensagem s�o obrigat�rios e devem existir se o prazo se aplica."
                    VIEW-AS ALERT-BOX ERROR BUTTONS OK.

                RETURN FALSE.
            END.
        END.
  END.
  
  RETURN TRUE.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

